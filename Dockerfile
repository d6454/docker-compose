FROM python:3.6
WORKDIR ./app
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt
COPY . /app
expose 5000
ENTRYPOINT ["sh", "entrypoint.sh"]